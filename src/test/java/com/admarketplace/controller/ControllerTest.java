package com.admarketplace.controller;

import com.admarketplace.model.User;
import com.admarketplace.request.AuthorizationRequest;
import com.admarketplace.request.RegistryRequest;
import com.admarketplace.service.UserService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class ControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    private Gson gson = new Gson();

    @Before
    public void create() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
              //  .setControllerAdvice(new GlobalExceptionController())
                .build();
    }

    @Test
    public void successfulRegistry() throws Exception {
        RegistryRequest registryRequest = new RegistryRequest("login", "password");
        doNothing().when(userService).registration(registryRequest.getLogin(), registryRequest.getPassword());
        mockMvc.perform(put("/registry")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(registryRequest)))
                .andExpect(status().isCreated());

        verify(userService, times(1)).registration(registryRequest.getLogin(), registryRequest.getPassword());
    }

    @Test
    public void successfulAuthorization() throws Exception {
        AuthorizationRequest authorizationRequest = new AuthorizationRequest("login", "password");
        when(userService.authorization(authorizationRequest.getLogin(), authorizationRequest.getPassword())).thenReturn(new User(1, "login", "password"));
        mockMvc.perform(get("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(authorizationRequest)))
                .andExpect(status().isOk());

        verify(userService, times(1)).authorization(authorizationRequest.getLogin(), authorizationRequest.getPassword());
    }
}
