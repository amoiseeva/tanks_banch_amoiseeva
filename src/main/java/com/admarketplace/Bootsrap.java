package com.admarketplace;

import com.admarketplace.config.DatabaseConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bootsrap {

    public static void main(String[] args) {
        SpringApplication.run(DatabaseConfig.class, args);
    }
}
