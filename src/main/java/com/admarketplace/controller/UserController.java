package com.admarketplace.controller;

import com.admarketplace.model.User;
import com.admarketplace.request.AuthorizationRequest;
import com.admarketplace.request.RegistryRequest;
import com.admarketplace.response.AuthorizationResponse;
import com.admarketplace.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/registry")
    @ResponseStatus(HttpStatus.CREATED)
    public void registration(@RequestBody RegistryRequest registryRequest) {
        userService.registration(registryRequest.getLogin(), registryRequest.getPassword());
    }

    @GetMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public AuthorizationResponse authorization(@RequestBody AuthorizationRequest authorizationRequest) {
        User user = userService.authorization(authorizationRequest.getLogin(), authorizationRequest.getPassword());
        return new AuthorizationResponse(user.getId(), user.getLogin());
    }
}
