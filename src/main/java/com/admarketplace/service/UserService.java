package com.admarketplace.service;

import com.admarketplace.model.User;

public interface UserService {

    void registration(String name, String password);

    User authorization(String name, String password);
}
